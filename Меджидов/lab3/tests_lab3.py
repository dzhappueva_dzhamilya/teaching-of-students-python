import unittest
import lab3


class TestLab3(unittest.TestCase):
    def setUp(self):
        pass

    def test_three_str(self):
        self.assertEqual(lab3.three_str_and_len("123"), ("123,123,123", 11))
        self.assertEqual(lab3.three_str_and_len("1"), ("1,1,1", 5))
        self.assertEqual(lab3.three_str_and_len("str"), ("str,str,str", 11))
        self.assertEqual(lab3.three_str_and_len("str,"), ("str,,str,,str,", 14))


    def test_get_first_last_middle_symbol(self):
        self.assertIsNone(lab3.get_first_last_middle_symbol(""))
        self.assertEqual(lab3.get_first_last_middle_symbol("0"), ('0', '0', '0'))
        self.assertEqual(lab3.get_first_last_middle_symbol("123"), ('1', '3', '2'))
        self.assertEqual(lab3.get_first_last_middle_symbol("12"), ('1', '2', ''))


    def test_replace_abc(self):
        self.assertEqual(lab3.replace_abc("abc"), "www")
        self.assertEqual(lab3.replace_abc("ab"), "abzzz")
        self.assertEqual(lab3.replace_abc(""), "zzz")
        self.assertEqual(lab3.replace_abc("abcqwerty"), "wwwqwerty")


    def test_len_digit(self):
        self.assertEqual(lab3.len_digit("['1', '5', '4', '7']"), 4)
        self.assertEqual(lab3.len_digit("Process finished with exit code 0"), 1)
        self.assertEqual(lab3.len_digit("Testing started at 15:52 ..."), 4)
        self.assertEqual(lab3.len_digit("Ran 1 test in 0.000s"), 5)


    def test_is_palindrome(self):
        self.assertTrue(lab3.is_palindrome('121'))
        self.assertTrue(lab3.is_palindrome('1221'))
        self.assertTrue(lab3.is_palindrome('777'))
        self.assertTrue(lab3.is_palindrome('Madam'))
        self.assertTrue(lab3.is_palindrome('Saippuakivikauppias'))
        self.assertFalse(lab3.is_palindrome('Saippua4kivikauppias'))
        self.assertFalse(lab3.is_palindrome('778'))
        self.assertFalse(lab3.is_palindrome('12345'))
        self.assertFalse(lab3.is_palindrome('mama'))

    def test_roman_number_to_decimal(self):
        self.assertEqual(lab3.roman_number_to_decimal('I'), 1)
        self.assertEqual(lab3.roman_number_to_decimal('II'), 2)
        self.assertEqual(lab3.roman_number_to_decimal('III'), 3)
        self.assertEqual(lab3.roman_number_to_decimal('IV'), 4)
        self.assertEqual(lab3.roman_number_to_decimal('V'), 5)
        self.assertEqual(lab3.roman_number_to_decimal('VI'), 6)
        self.assertEqual(lab3.roman_number_to_decimal('VII'), 7)
        self.assertEqual(lab3.roman_number_to_decimal('VIII'), 8)
        self.assertEqual(lab3.roman_number_to_decimal('IX'), 9)
        self.assertEqual(lab3.roman_number_to_decimal('X'), 10)
        self.assertEqual(lab3.roman_number_to_decimal('XI'), 11)
        self.assertEqual(lab3.roman_number_to_decimal('XII'), 12)
        self.assertEqual(lab3.roman_number_to_decimal('XIII'), 13)
        self.assertEqual(lab3.roman_number_to_decimal('XIV'), 14)
        self.assertEqual(lab3.roman_number_to_decimal('XV'), 15)
        self.assertEqual(lab3.roman_number_to_decimal('XVI'), 16)
        self.assertEqual(lab3.roman_number_to_decimal('XVII'), 17)
        self.assertEqual(lab3.roman_number_to_decimal('XVIII'), 18)
        self.assertEqual(lab3.roman_number_to_decimal('XIX'), 19)
        self.assertEqual(lab3.roman_number_to_decimal('XX'), 20)
        self.assertEqual(lab3.roman_number_to_decimal('XXI'), 21)
        self.assertEqual(lab3.roman_number_to_decimal('XXII'), 22)
        self.assertEqual(lab3.roman_number_to_decimal('XXIII'), 23)
        self.assertEqual(lab3.roman_number_to_decimal('XXIV'), 24)
        self.assertEqual(lab3.roman_number_to_decimal('XXV'), 25)
        self.assertEqual(lab3.roman_number_to_decimal('XXVI'), 26)
        self.assertEqual(lab3.roman_number_to_decimal('XXVII'), 27)
        self.assertEqual(lab3.roman_number_to_decimal('XXVIII'), 28)
        self.assertEqual(lab3.roman_number_to_decimal('XXIX'), 29)
        self.assertEqual(lab3.roman_number_to_decimal('XXX'), 30)
        self.assertEqual(lab3.roman_number_to_decimal('XXXI'), 31)
        self.assertEqual(lab3.roman_number_to_decimal('XXXII'), 32)
        self.assertEqual(lab3.roman_number_to_decimal('XXXIII'), 33)
        self.assertEqual(lab3.roman_number_to_decimal('XXXIV'), 34)
        self.assertEqual(lab3.roman_number_to_decimal('XXXV'), 35)
        self.assertEqual(lab3.roman_number_to_decimal('XXXVI'), 36)
        self.assertEqual(lab3.roman_number_to_decimal('XXXVII'), 37)
        self.assertEqual(lab3.roman_number_to_decimal('XXXVIII'), 38)
        self.assertEqual(lab3.roman_number_to_decimal('XXXIX'), 39)
        self.assertEqual(lab3.roman_number_to_decimal('XL'), 40)
        self.assertEqual(lab3.roman_number_to_decimal('XLI'), 41)
        self.assertEqual(lab3.roman_number_to_decimal('XLII'), 42)
    
    def test_decimal_to_roman_number(self):
        self.assertEqual(lab3.decimal_to_roman_number(1), 'I')
        self.assertEqual(lab3.decimal_to_roman_number(2), 'II')
        self.assertEqual(lab3.decimal_to_roman_number(3), 'III')
        self.assertEqual(lab3.decimal_to_roman_number(4), 'IV')
        self.assertEqual(lab3.decimal_to_roman_number(5), 'V')
        self.assertEqual(lab3.decimal_to_roman_number(6), 'VI')
        self.assertEqual(lab3.decimal_to_roman_number(7), 'VII')
        self.assertEqual(lab3.decimal_to_roman_number(8), 'VIII')
        self.assertEqual(lab3.decimal_to_roman_number(9), 'IX')
        self.assertEqual(lab3.decimal_to_roman_number(10), 'X')
        self.assertEqual(lab3.decimal_to_roman_number(11), 'XI')
        self.assertEqual(lab3.decimal_to_roman_number(12), 'XII')
        self.assertEqual(lab3.decimal_to_roman_number(13), 'XIII')
        self.assertEqual(lab3.decimal_to_roman_number(14), 'XIV')
        self.assertEqual(lab3.decimal_to_roman_number(15), 'XV')
        self.assertEqual(lab3.decimal_to_roman_number(16), 'XVI')
        self.assertEqual(lab3.decimal_to_roman_number(17), 'XVII')
        self.assertEqual(lab3.decimal_to_roman_number(18), 'XVIII')
        self.assertEqual(lab3.decimal_to_roman_number(19), 'XIX')
        self.assertEqual(lab3.decimal_to_roman_number(20), 'XX')
        self.assertEqual(lab3.decimal_to_roman_number(21), 'XXI')
        self.assertEqual(lab3.decimal_to_roman_number(22), 'XXII')
        self.assertEqual(lab3.decimal_to_roman_number(23), 'XXIII')
        self.assertEqual(lab3.decimal_to_roman_number(24), 'XXIV')
        self.assertEqual(lab3.decimal_to_roman_number(25), 'XXV')
        self.assertEqual(lab3.decimal_to_roman_number(26), 'XXVI')
        self.assertEqual(lab3.decimal_to_roman_number(27), 'XXVII')
        self.assertEqual(lab3.decimal_to_roman_number(28), 'XXVIII')
        self.assertEqual(lab3.decimal_to_roman_number(29), 'XXIX')
        self.assertEqual(lab3.decimal_to_roman_number(30), 'XXX')
        self.assertEqual(lab3.decimal_to_roman_number(31), 'XXXI')
        self.assertEqual(lab3.decimal_to_roman_number(32), 'XXXII')
        self.assertEqual(lab3.decimal_to_roman_number(33), 'XXXIII')
        self.assertEqual(lab3.decimal_to_roman_number(34), 'XXXIV')
        self.assertEqual(lab3.decimal_to_roman_number(35), 'XXXV')
        self.assertEqual(lab3.decimal_to_roman_number(36), 'XXXVI')
        self.assertEqual(lab3.decimal_to_roman_number(37), 'XXXVII')
        self.assertEqual(lab3.decimal_to_roman_number(38), 'XXXVIII')
        self.assertEqual(lab3.decimal_to_roman_number(39), 'XXXIX')
        self.assertEqual(lab3.decimal_to_roman_number(40), 'XL')
        self.assertEqual(lab3.decimal_to_roman_number(41), 'XLI')
        self.assertEqual(lab3.decimal_to_roman_number(42), 'XLII')

if __name__ == '__main__':
    unittest.main()
